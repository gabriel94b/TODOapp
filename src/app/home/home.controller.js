'use strict';
export default class HomeTasksCtrl {

	constructor($rootScope, $scope, tasksService) {
    this.$rootScope = $rootScope;
    this.$scope = $scope;
		this.tasksService = tasksService;

		//sorting
		this.orderByColumn = 'id';
		this.orderByDir = false;

		//pagging
		this.currentPage = 1;
		this.paggingOptions = [5,10,15];
		this.recordsPerPage = this.paggingOptions[0];

		//statusOptions
		this.statusOptions = ['Low','Medium','High'];
		this.newPriority = this.statusOptions[0];

		this.tasks = [];
		this.allTasks = this.tasksService.getTasks();
		this.changePage(this.currentPage);

	}

	addTask() {
		const date = new Date();
		this.allTasksTemp = JSON.parse(window.localStorage.getItem('allTasks')) || [];
		this.allTasksTemp.push({
			id: date.getTime(),
			taskName: this.newTaskName,
			priority: this.newPriority || 'Low',
			done: this.newDone || false
		});

		this.newTaskName = null;
		this.newPriority = 'Low';
		this.newDone = false;
		this.refresh(this.allTasksTemp);
	}

	updateTask(taskId, taskDone) {
		this.allTasks = JSON.parse(window.localStorage.getItem('allTasks')) || [];
		if(this.allTasks) {
			for (var i in this.allTasks) {
	     if (this.allTasks[i].id === taskId) {
	        this.allTasks[i].done = taskDone;
	        break;
     		}
   		}
			this.refresh(this.allTasks);
		}
	}

	deleteTask(taskId) {
		this.allTasks = JSON.parse(window.localStorage.getItem('allTasks')) || [];
		for (let i = 0; i < this.allTasks.length; i++) {
	    if (this.allTasks[i].id && this.allTasks[i].id === taskId) {
	        this.allTasks.splice(i, 1);
	        break;
	    }
		}
		this.refresh(this.allTasks);
	}

	refresh(tasksList) {
		window.localStorage.setItem('allTasks', JSON.stringify(tasksList));
		this.allTasks = JSON.parse(window.localStorage.getItem('allTasks'));
		this.changePage(this.currentPage);
	}

	changeRecords() {
		this.changePage(this.currentPage)
	}

	changeOrder(columnName) {
		if(this.orderByColumn === columnName) {
			this.orderByDir = !this.orderByDir;
		}
		else {
			this.orderByColumn = columnName;
		}
	}

	isOrderedBy(columnName) {
		return (this.orderByColumn === columnName);
	}

	isOrderedRevers(columnName) {
		return !this.orderByDir;
	}

	prevPage() {
    if (this.currentPage > 1) {
    	this.currentPage--;
      this.changePage(this.currentPage);
    }
	}

	nextPage() {
    if (this.currentPage < this.numPages()) {
      this.currentPage++;
      this.changePage(this.currentPage);
    }
	}

	changePage(page) {
    // Validate page
		let counterTask = 0;
		this.tasks = [];
    if (page < 1) page = 1;
    if (page > this.numPages()) page = this.numPages();

    for (let i = (page-1) * this.recordsPerPage; i < (page * this.recordsPerPage) && i < this.allTasks.length; i++) {
			this.firstTask = (page-1) * this.recordsPerPage+1;
			this.lastTask = i+1;
			this.tasks[counterTask] = this.allTasks[i];
			counterTask++;
    }

    if (page == 1) {
			this.prev = true;
    } else {
			this.prev = false;
    }

    if (page == this.numPages()) {
			this.next = true;
    } else {
			this.next = false;
    }
	}

	numPages() {
	    return Math.ceil(this.allTasks.length / this.recordsPerPage);
	}
}

HomeTasksCtrl.$inject = ['$rootScope', '$scope', 'tasksService'];
