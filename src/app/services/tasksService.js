'use strict';

export default class tasksService {

  constructor($q) {

  }

  getTasks() {
    const allTasksFromLocalStorage = JSON.parse(window.localStorage.getItem('allTasks')) || [];
		return allTasksFromLocalStorage;
	}


}

tasksService.$inject = ['$q'];
