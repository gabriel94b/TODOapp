'use strict';

//import styles css/scss
import 'bootstrap/dist/css/bootstrap.css';
import './app.style.scss';

//import modules
import angular from 'angular';
import uirouter from 'angular-ui-router';
import uibootstrap from 'angular-ui-bootstrap';

//import config
import AppConfig from './app.config';

//import controllers
import HomeTasksCtrl from './home/home.controller'

//import services
import TasksService from './services/tasksService'


//init toDoApp
export default angular.module('toDoApp', [uirouter, uibootstrap])
	.config(AppConfig)
	.controller('HomeTasksCtrl', HomeTasksCtrl)
	.service('tasksService', TasksService)
