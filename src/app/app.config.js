'use strict';
export default function AppConfig($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider
		.state('home', {
			url : '/',
			template: require('./home/home.template.html'),
			controller: 'HomeTasksCtrl',
			controllerAs: 'vm'
		});

	$urlRouterProvider.otherwise('/');
	$locationProvider.html5Mode(true);
}

AppConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
