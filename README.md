# ANGULARJS - TODO list app

# Example - [Demo](http://todo.gabrielbarszczowski.pl/)

## Build Setup

```bash
# clone repository
$ git clone git@gitlab.com:gabriel94b/TODOapp.git

# open app folder
$ cd TODOapp

# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ **npm start**

# clean dist folder
$ npm run clean

# build
$ npm run build
```

Server started at: [localhost:8080](localhost:8080)

*2018 MIT*
